import React, { Component } from 'react';
import MisLenguajes from './MisLenguajes';
import Template from './Template';


export class Home extends Component {
  static displayName = Home.name;

  render () {
    return (
      <div>
        <h1>XIPE LANGUAGE'S PROFICIENCY</h1>
        <MisLenguajes />
        <Template/>
      </div>
    );
  }
}
