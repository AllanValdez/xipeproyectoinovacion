import React, { Component } from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';

import { Route } from "react-router-dom";

import BarChart from "./common/BarChart";
import ColumnChart from "./common/ColumnChart";
import LineChart from "./common/LineChart";
import PieChart from "./common/PieChart";
import ScatterChart from "./common/ScatterChart";

class Template extends Component {
  
    render() {    
      return (
				<Col xl={{ span: 8, offset: 2 }} lg={{ span: 8, offset: 2 }} xs={{ span: 8, offset: 2 }}>
          <Container>
            <div className="content">
              <Route exact path="/" component={ColumnChart}/>
              <Route exact path="/" component={BarChart}/>
              <Route exact path="/" component={PieChart}/>
              <Route exact path="/" component={LineChart}/>
              <Route exact path="/" component={ScatterChart}/>
            </div>
          </Container>
        </Col>
      );
    }
  }
  
  export default Template;