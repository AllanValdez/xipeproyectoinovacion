import React from 'react';
import styled from 'styled-components';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

const MisLenguajes = () => {

    return (
        <Container>
            <Form>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="nombre" placeholder="Type your name here..." />
                    <Form.Text className="text-muted">
                        Just so we can identify you
    </Form.Text>
                </Form.Group>
                    <Form.Group controlId="formGridState">
      <Form.Label>Language</Form.Label>
      <Form.Control as="select">
        <option>C#</option>
        <option>JavaScrip</option>
        <option>PHP</option>
        <option>Go</option>
        <option>Python</option>
        <option>latin</option>
        <option>Java</option>
        <option>haskell</option>
        <option>html</option>
      </Form.Control>
    </Form.Group>

                <Button variant="primary" type="submit">
                    Submit
  </Button>
            </Form>
        </Container>
    );
}

const Container = styled.div`

`;


export default MisLenguajes;
