import React, { Component } from 'react';
import CanvasJSReact from '../../assets/canvasjs.react';
import axios from  'axios';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
var CanvasJS = CanvasJSReact.CanvasJS;

class BarChart extends Component {
	state = {
		raza: null
	}
	addSymbols(e){
		var suffixes = ["", "K", "M", "B"];
		var order = Math.max(Math.floor(Math.log(e.value) / Math.log(1000)), 0);
		if(order > suffixes.length - 1)
			order = suffixes.length - 1;
		var suffix = suffixes[order];
		return CanvasJS.formatNumber(e.value / Math.pow(1000, order)) + suffix;
		
	}
	async componentDidMount() {
		const data = await axios.get('api/workers/getlist');
		console.log(data);
		this.setState({raza: data.data});
	  }
	render() {
		const raza = this.state.raza;
		const opts = [];
		console.log(raza);
		// raza.Skills.map(i => {
		// 	opts.push({
		// 		y: i.range,
		// 		label: i.skill
		// 	});
		// })
		// console.log(opts);
		const options = {
			animationEnabled: true,
			theme: "light2",
			title:{
				text: "Bar Chart"
			},
			axisX: {
				title: "Language Proficiency Percentage",
				reversed: true,
			},
			axisY: {
				title: "Language",
				labelFormatter: this.addSymbols
			},
			data: [{
				type: "bar",
				// dataPoints: opts
				dataPoints: [
					{ y:  50, label: "C#" },
					{ y:  20, label: "Python" },
					{ y:  15, label: "Javascript" },
					{ y:  10, label: "Angular" },
				]
			}]
		}
		
		return (
		<div>
			<CanvasJSChart options = {options} 
				/* onRef={ref => this.chart = ref} */
			/>
			{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
		</div>
		);
	}
}

export default BarChart;