import React, { Component } from 'react';
import CanvasJSReact from '../../assets/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
 
class ScatterChart extends Component {
	render() {
		const options = {
			theme: "light2",
			animationEnabled: true,
			zoomEnabled: true,
			title:{
				text: "Scatter Chart"
			},
			axisX: {
				title:"Language",
				crosshair: {
					enabled: true,
					snapToDataPoint: true
				}
			},
			axisY:{
				title: "Language Proficiency Percentage",
				suffix: "%",
				includeZero: false,
				crosshair: {
					enabled: true,
					snapToDataPoint: true
				}
			},
			data: [{
				type: "scatter",
				markerSize: 15,
				toolTipContent: "<b>Language: </b>{x}°C<br/><b>Proficiency: </b>{y}",
				dataPoints: [
					{ x: 1, y: 55},
					{ x: 2, y: 20},
					{ x: 3, y: 15},
					{ x: 4, y: 10}
				]
			}]
		}
		
		return (
		<div>
			<CanvasJSChart options = {options} 
				/* onRef={ref => this.chart = ref} */
			/>
			{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
		</div>
		);
	}
}

export default ScatterChart;