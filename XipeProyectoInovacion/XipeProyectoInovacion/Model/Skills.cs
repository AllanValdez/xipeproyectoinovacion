﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XipeProyectoInovacion.Model
{
	public class Skills
	{
		public string Skill { get; set; }
		public int Range { get; set; }
	}
}
