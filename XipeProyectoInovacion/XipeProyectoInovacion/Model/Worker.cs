﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XipeProyectoInovacion.Model;

namespace XipeProyectoInovacion.WebService
{
    public class Worker
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Skills> Skills { get; set; }
    }
}
