﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using XipeProyectoInovacion.Model;
using XipeProyectoInovacion.WebService;

namespace XipeProyectoInovacion.Controllers
{
    [Route("api/workers")]
    public class WorkerController : Controller
    {
        private WorkerSearch _DataService;

        public IActionResult Index()
        {
            return View();
        }


        [HttpGet("getlist")]
        public IActionResult GetList()
        {
            List<Worker> workers = new List<Worker>();
            workers.Add(new Worker
            {
                Name = "Allan",
                Skills = new List<Skills>
                {
                    new Skills { Skill = "c#", Range = 34 },
                    new Skills { Skill = "python", Range = 56},
                    new Skills { Skill = "javascript", Range = 84},
                    new Skills { Skill = "angular", Range = 74}
                }
            });
            // workers.Add(new Worker
            // {
            //     Name = "Many",
            //     Skills = new List<Skills>
            //     {
            //         new Skills { Skill = "c#", Range = 74 },
            //         new Skills { Skill = "python", Range = 12 },
            //         new Skills { Skill = "javascript", Range = 95},
            //         new Skills { Skill = "angular", Range = 26}
            //     }
            // });
            // workers.Add(new Worker
            // {
            //     Name = "Toche",
            //     Skills = new List<Skills>
            //     {
            //         new Skills { Skill = "c#", Range = 65 },
            //         new Skills { Skill = "python", Range = 23},
            //         new Skills { Skill = "javascript", Range = 67},
            //         new Skills { Skill = "angular", Range = 12}
            //     }
            // });
            // workers.Add(new Worker
            // {
            //     Name = "Franco",
            //     Skills = new List<Skills>
            //     {
            //         new Skills { Skill = "c#", Range = 42 },
            //         new Skills { Skill = "python", Range = 69},
            //         new Skills { Skill = "javascript", Range = 45},
            //         new Skills { Skill = "angular", Range = 2}
            //     }
            // });

            return Ok(new Worker
            {
                Name = "Allan",
                Skills = new List<Skills>
                {
                    new Skills { Skill = "c#", Range = 34 },
                    new Skills { Skill = "python", Range = 56},
                    new Skills { Skill = "javascript", Range = 84},
                    new Skills { Skill = "angular", Range = 74}
                }
            });
        }
    }
}